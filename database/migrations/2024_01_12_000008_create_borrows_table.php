<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrows', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id_borrower');
            $table->foreign('user_id_borrower')->references('id')->on('users');
            $table->unsignedBigInteger('user_id_approver')->nullable();
            $table->foreign('user_id_approver')->references('id')->on('users');
            $table->unsignedBigInteger('user_id_giver')->nullable();
            $table->foreign('user_id_giver')->references('id')->on('users');
            $table->unsignedBigInteger('user_id_receiver')->nullable();
            $table->foreign('user_id_receiver')->references('id')->on('users');
            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items');
            $table->smallInteger('qty_borrowed')->default(1);
            $table->smallInteger('qty_returned')->default(0);
            $table->dateTime('borrow_date');
            $table->dateTime('return_date')->nullable();
            $table->date('deadline_date');
            $table->text('note')->nullable();
            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('borrow_status');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrows');
    }
};
