<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\LabController;
use App\Http\Controllers\Api\ItemController;
use App\Http\Controllers\Api\BorrowStatusController;
use App\Http\Controllers\Api\BorrowController;
use App\Http\Controllers\Api\LogController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Public routes
Route::post('/role/store', [RoleController::class, 'store']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

//Protected routes
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/verify', [AuthController::class, 'checkIfAuthenticated']);
    Route::get('/log', [LogController::class, 'index']);

    Route::group(['prefix' => 'user'], function (){
        Route::get('/user-all', [UserController::class, 'getUserAll']);
        Route::get('/user-by-role', [UserController::class, 'getUserRole']);
        Route::post('/add-user-lab', [UserController::class, 'addUserLab']);
        Route::put('/blacklist/{id}', [UserController::class, 'blacklist']);
        Route::get('/show/{id}', [UserController::class, 'show']);
        Route::put('/edit/{id}', [UserController::class, 'update']);
        Route::put('/update-password/{id}', [UserController::class, 'updatePassword']);
    });

    Route::group(['prefix' => 'item'], function (){
        Route::get('/', [ItemController::class, 'index']);
        Route::get('/show/{id}', [ItemController::class, 'show']);
        Route::post('/store', [ItemController::class, 'store']);
        Route::post('/edit/{id}', [ItemController::class, 'update']);
        Route::delete('/delete/{id}', [ItemController::class, 'destroy']);
        Route::delete('/delete-image/{id}', [ItemController::class, 'deleteItemImage']);
    });

    Route::group(['prefix' => 'lab'], function (){
        Route::get('/', [LabController::class, 'index']);
        Route::get('/lab-per-user', [LabController::class, 'getByUser']);
        Route::get('/show/{id}', [LabController::class, 'show']);
        Route::get('/show-lab-per-user/{id}', [LabController::class, 'showPerUser']);
        Route::post('/store', [LabController::class, 'store']);
        Route::put('/edit/{id}', [LabController::class, 'update']);
        Route::delete('/delete/{id}', [LabController::class, 'destroy']);
    });

    Route::group(['prefix' => 'borrow'], function (){
        Route::get('/', [BorrowController::class, 'index']);
        Route::get('/list-pinjam-per-user', [BorrowController::class, 'listPinjamPerUser']);
        Route::get('/list-pinjam', [BorrowController::class, 'listPinjam']);
        // Route::get('/show/{id}', [BorrowController::class, 'show']);
        Route::post('/store', [BorrowController::class, 'store']);
        Route::put('/edit/{id}', [BorrowController::class, 'update']);
        // Route::delete('/delete/{id}', [BorrowController::class, 'destroy']);
    });

    Route::group(['prefix' => 'borrow-status'], function (){
        Route::get('/', [BorrowStatusController::class, 'index']);
        Route::post('/store', [BorrowStatusController::class, 'store']);
        Route::post('/borrow', [BorrowStatusController::class, 'borrow']);
        Route::put('/edit/{id}', [BorrowStatusController::class, 'update']);
        Route::delete('/delete/{id}', [BorrowStatusController::class, 'destroy']);
    });

});
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
