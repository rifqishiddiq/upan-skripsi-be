<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\Borrow;
use App\Models\Item;
use App\Models\Lab;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\UserLab;
use App\Models\User;
use App\Traits\log;

class BorrowController extends Controller
{
    use log, HttpResponses;

    public function index()
    {
        return $this->successResponse(Borrow::with('item', 'borrower', 'approver', 'laboran_giver', 'laboran_receiver', 'status')->get());
    }

    public function show($id)
    {
        return $this->successResponse(Item::with('lab')->find($id));
    }

    public function listPinjamPerUser(Request $request)
    {
        $user = auth()->user();
        $request->status_id = '1,2,3,4,5,6,7';
        $param_status = explode(",", $request->status_id);

        $borrow = Borrow::with('item', 'borrower', 'approver', 'laboran_giver', 'laboran_receiver', 'status')
            ->where('user_id_borrower', $user->id)
            ->whereIn('status_id', $param_status)
            ->orderBy('updated_at','desc')
            ->get();
        return $this->successResponse($borrow, "Fetch Data Success!");
    }

    public function listPinjam(Request $request)
    {
        $user = auth()->user();
        // check role
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        $request->status_id = '1,2,3,4,5,6,7';
        $param_status = explode(",", $request->status_id);

        if ($role->name === 'admin') {
            $borrow = Borrow::with('item', 'item.lab', 'borrower', 'approver', 'laboran_giver', 'laboran_receiver', 'status')
                ->whereIn('status_id', $param_status)
                ->orderBy('updated_at','desc')
                ->get();
            }
            else if($role->name === 'dosen') {
            $user_lab = UserLab::where('user_id', $user->id)->get();
            if (!$user_lab) {
                return $this->errorResponse('', 'User does not have the access to this function!', 400);
            }
            $list_lab_id = array();
            foreach ($user_lab as $x) {
                array_push($list_lab_id, $x['lab_id']);
            }
            
            $borrow = Borrow::with('item', 'item.lab', 'borrower', 'approver', 'laboran_giver', 'laboran_receiver', 'status')
                ->whereHas('item', function($q) use ($list_lab_id) {
                    $q->whereIn('lab_id', $list_lab_id);
                })
                ->whereIn('status_id', $param_status)
                ->orderBy('updated_at','desc')
                ->get();
        }
        else {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        return $this->successResponse($borrow, "Fetch Data Success!");
    }

    public function store(Request $request)
    {
        $user = auth()->user();

        // Validation
        $this->validate($request, [
            // 'user_id_borrower' => 'required|exists:users,id',
            // 'user_id_approved' => 'nullable|exists:users,id',
            // 'user_id_giver' => 'nullable|exists:users,id',
            // 'user_id_receiver' => 'nullable|exists:users,id',
            'item_id' => 'required|exists:items,id',
            'qty_borrowed' => 'required|numeric',
            // 'qty_returned' => 'nullable|numeric',
            'borrow_date' => 'required|after_or_equal:today',
            // 'return_date' => 'required',
            // 'deadline_date' => 'required',
            // 'note' => 'required',
            // 'status_id' => 'required',
        ]);
        
        try
        {
            $item = Item::findOrFail($request->item_id);
            if ($item->stock === 0) {
                return $this->errorResponse('', 'Stok tidak tersedia!', 400);
            }
            if ($request->qty_borrowed > $item->stock) {
                return $this->errorResponse('', 'Pinjaman melebihi stok yang tersedia!', 400);
            }
            $deadline_date = date('Y-m-d',strtotime($request->borrow_date . " +30 days"));

            $borrow = Borrow::create([
                'user_id_borrower' => $user->id,
                'user_id_approver' => null,
                'user_id_giver' => null,
                'user_id_receiver' => null,
                'item_id' => $request->item_id,
                'lab_id' => $request->lab_id,
                'qty_borrowed' => $request->qty_borrowed,
                'qty_returned' => 0,
                'borrow_date' => $request->borrow_date,
                'return_date' => null,
                'deadline_date' => $deadline_date,
                'note' => null,
                'status_id' => 1,
            ]);

            $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], null, json_encode($request->all()), '-');

            $item = Item::find($request->item_id);
            $item->remaining = $item->remaining - $request->qty_borrowed;
            try {
                $item->save();
            } catch (\Throwable $th) {
                return $this->errorResponse('', 'Update data Item gagal!', 400);
            }

            return $this->successResponse([
                'transaksi' => $borrow
            ], "Create Data Success!");
        }
        // catch(Exception $e) catch any exception
        catch(ModelNotFoundException $e)
        {
            return $this->errorResponse('', 'Item tidak ditemukan!', 400);
        }

    }

    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $this->validate($request, [
            'action' => 'required',
        ]);

        $borrow = Borrow::with('status', 'item')->find($id);
        $lab = Lab::with('pic')->find($borrow->item->lab_id);
        $user_lab = UserLab::where('user_id', $user->id)->where('lab_id', $borrow->item->lab_id)->first();
        $user_id_approver = null;
        $user_id_giver = null;
        $user_id_receiver = null;
        $qty_returned = 0;
        $borrow_status = 0;
        $return_date = null;
        $note = null;
        
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        
        switch ($request->action) {
            case 'disetujui':
                if ($user->id !== $lab->user_id_pic) {
                    return $this->errorResponse('', 'User does not have the access to this function!', 400);
                }
                if ($borrow->status['name'] !== 'pengajuan') {
                    return $this->errorResponse('', 'Status peminjaman tidak dapat disetujui!', 400);
                }
                $borrow_status = 2;
                $user_id_approver = $user->id;
                break;
            case 'ditolak':
                if ($user->id !== $lab->user_id_pic) {
                    return $this->errorResponse('', 'User does not have the access to this function!', 400);
                }
                if ($borrow->status['name'] !== 'pengajuan') {
                    return $this->errorResponse('', 'Status peminjaman tidak dapat ditolak!', 400);
                }
                $this->validate($request, [
                    'note' => 'required',
                ]);
                $note = $request->note;

                $item = Item::find($borrow->item_id);
                $item->remaining = $item->remaining + $borrow->qty_borrowed;
                try {
                    $item->save();
                } catch (\Throwable $th) {
                    return $this->errorResponse('', 'Update data Item gagal!', 400);
                }
                $borrow_status = 3;
                $user_id_approver = $user->id;
                break;
            case 'dipinjam':
                if (!$user_lab) {
                    return $this->errorResponse('', 'User does not have the access to this function!', 400);
                }
                $borrow_status = 4;
                $user_id_approver = $borrow->user_id_approver;
                $user_id_giver = $user->id;
                break;
            case 'dikembalikan':
                if (!$user_lab) {
                    return $this->errorResponse('', 'User does not have the access to this function!', 400);
                }
                $this->validate($request, [
                    'qty_returned' => 'required|numeric|min:1',
                ]);
                if (!($borrow->status['name'] === 'pengajuan' OR $borrow->status['name'] === 'disetujui')) {
                    return $this->errorResponse('', 'Peminjaman sudah diproses dan tidak bisa dibatalkan!', 400);
                }
                $item = Item::find($borrow->item_id);
                $item->remaining = $item->remaining + $request->qty_returned;
                try {
                    $item->save();
                } catch (\Throwable $th) {
                    return $this->errorResponse('', 'Update data Item gagal!', 400);
                }

                $borrow_status = 5;
                $user_id_approver = $borrow->user_id_approver;
                $user_id_giver = $borrow->user_id_giver;
                $user_id_receiver = $user->id;
                $return_date = date('Y-m-d H:i:s');
                $qty_returned = $request->qty_returned;
                $note = $request->note;
                break;
            case 'dibatalkan':
                if (!($borrow->user_id_borrower === $user->id || $user->id === $lab->user_id_pic)) {
                    return $this->errorResponse('', 'User does not have the access to this function!', 400);
                }
                if (!($borrow->status['name'] === 'pengajuan' || $borrow->status['name'] === 'disetujui')) {
                    return $this->errorResponse('', 'Peminjaman sudah tidak bisa dibatalkan!', 400);
                }
                $this->validate($request, [
                    'note' => 'required',
                ]);
                $note = $request->note;
                $borrow_status = 6;
                $user_id_approver = $borrow->$user_id_approver;

                $item = Item::find($borrow->item_id);
                $item->remaining = $item->remaining + $borrow->qty_borrowed;
                try {
                    $item->save();
                } catch (\Throwable $th) {
                    return $this->errorResponse('', 'Update data Item gagal!', 400);
                }
                break;
            case 'gagal':
                if (!$user_lab) {
                    return $this->errorResponse('', 'User does not have the access to this function!', 400);
                }
                if ($borrow->status['name'] !== 'dipinjam') {
                    return $this->errorResponse('', 'Peminjaman sudah tidak bisa digagalkan!', 400);
                }
                $this->validate($request, [
                    'note' => 'required',
                ]);
                $note = $request->note;
                $user_id_approver = $borrow->$user_id_approver;
                $user_id_giver = $borrow->user_id_giver;
                $borrow_status = 7;
                break;
            default:
                return $this->errorResponse($request->all(), 'Action not found!', 400);
                break;
        }
        $borrow->user_id_approver = $user_id_approver;
        $borrow->user_id_giver = $user_id_giver;
        $borrow->user_id_receiver = $user_id_receiver;
        $borrow->qty_returned = $qty_returned;
        $borrow->return_date = $return_date;
        $borrow->status_id = $borrow_status;
        $borrow->note = $note;
        $borrow->save();

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');
        
        $new_borrow = Borrow::with('status', 'item')->find($id);
        return $this->successResponse([
            'transaksi' => $new_borrow
        ], "Update Data Success!");
    }

    public function destroy(Request $request, $id)
    {
        return $this->successResponse([], "none!");
    }
}
