<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\BorrowStatus;
use App\Models\Role;
use App\Models\UserRole;

class BorrowStatusController extends Controller
{
    use HttpResponses;

    public function index()
    {
        return $this->successResponse(BorrowStatus::all());
    }

    public function store(Request $request)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        if ($role->name !== 'admin') {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // Validation
        $this->validate($request, [
            'name' => 'required|max:12',
        ]);

        try
        {
            $borrow_status = BorrowStatus::create([
                'name' => $request->name,
            ]);

            return $this->successResponse([
                'status' => $borrow_status
            ], "Create Data Success!");
        }
        // catch(Exception $e) catch any exception
        catch(ModelNotFoundException $e)
        {
            return $this->errorResponse('', 'Create Data Gagal!', 400);
        }
    }

    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        if ($role->name !== 'admin') {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // Validation
        $this->validate($request, [
            'name' => 'required|max:12',
        ]);

        $borrow_status = BorrowStatus::find($id);
        $borrow_status->name = $request->name;
        $borrow_status->save();

        return $this->successResponse([
            'status' => $borrow_status
        ], "Update Data Success!");
    }

    public function destroy(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        if ($role->name !== 'admin') {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        $borrow_status = BorrowStatus::find($id);

        $borrow_status->delete();
        return $this->successResponse([], "Delete Data Success!");
    }
}
