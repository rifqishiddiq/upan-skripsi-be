<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use App\Models\Role;

class RoleController extends Controller
{
    use HttpResponses;

    public function index()
    {
        return $this->success(Role::all());
    }

    public function store(Request $request)
    {
        // Validation
        $this->validate($request, [
            'name' => 'required|unique:roles',
        ]);

        $role = Role::create([
            'name' => $request->name,
        ]);

        return $this->successResponse([
            'role' => $role,
        ]);
    }
}
