<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Role;
use App\Models\Lab;
use App\Models\UserLab;
use App\Models\UserRole;
use App\Traits\log;

class UserController extends Controller
{
    use log, HttpResponses;

    public function getUserAll(Request $request)
    {
        return $this->successResponse(User::with('user_role')->get());
    }

    public function getUserRole(Request $request)
    {
        return $this->successResponse(User::whereHas('user_role', function($q) use ($request) {
            $q->where('role_id', $request->role_id);
        })->get());
    }

    public function show($id)
    {
        return $this->successResponse(User::find($id));
    }

    public function update(Request $request, $id)
    {
        $user = auth()->user();

        // Validation
        $this->validate($request, [
            'name' =>'required|max:255',
            'phone' => ['required', 'numeric', 'digits_between:3,15', Rule::unique('users')->ignore($id)],
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($id)],
        ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->save();

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');

        return $this->successResponse([
            'user' => $user
        ], "Update User Success!");
    }

    public function updatePassword(Request $request, $id)
    {
        $user = auth()->user();

        $user = User::find($id);
        $user->password = bcrypt($request->password);
        $user->save();

        // $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');
        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, '', '-');

        return $this->successResponse([
            'user' => $user
        ], "Update User Success!");
    }

    public function addUserLab(Request $request)
    {
        $this->validate($request, [
            'user_id' =>'required|exists:users,id',
            'lab_id' =>'required|exists:labs,id',
        ]);

        $user_role = UserRole::where('user_id', $request->user_id)->where(function($query) {
                $query->where('role_id', 2);
            })->first();

        if (!$user_role) {
            return $this->errorResponse('', 'user not found / user is not Dosen', 400);
        }

        $user_lab = userLab::create([
            'user_id' => $request->user_id,
            'lab_id' => $request->lab_id,
        ]);

        return $this->successResponse([
            'user_lab' => $user_lab,
        ]);
    }

    public function blacklist(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        if ($role->name !== 'admin' ) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        $user = User::find($id);

        $user->blacklisted = $request->blacklisted;
        $user->blacklisted_note = $request->blacklisted_note;
        $user->save();

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');

        return $this->successResponse([
            'user' => $user
        ], "Update Data Success!");
    }
}
