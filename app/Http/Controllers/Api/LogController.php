<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use App\Models\Log;

class LogController extends Controller
{
    use HttpResponses;

    public function index()
    {
        return $this->successResponse(Log::with('creator')->orderBy('id', 'DESC')->get());
    }
}
