<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\Role;
use App\Models\UserLab;
use App\Models\UserRole;
use App\Models\Lab;
use App\Models\Item;
use App\Models\Borrow;
use App\Traits\log;

class LabController extends Controller
{
    use log, HttpResponses;

    public function index()
    {
        return $this->successResponse(Lab::with('pic')->get());
    }

    public function getByUser(Request $request)
    {
        $user = auth()->user();
        if ($request->role_id === 1 OR $request->role_id === "1") {
            return $this->successResponse(Lab::with('pic', 'user_lab')->get());
        }
        else {
            return $this->successResponse(Lab::with('pic', 'user_lab')->whereHas('user_lab', function($q) use ($user) {
                $q->where('user_id', $user->id);
            })->get());
        }
    }

    public function show($id)
    {
        return $this->successResponse(Lab::with('pic')->find($id));
    }

    public function showPerUser($id)
    {
        $user = auth()->user();
        return $this->successResponse(Lab::with('pic', 'user_lab')->find($id));
    }

    // ONLY ADMIN
    public function store(Request $request)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        if ($role->name !== 'admin' ) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // Validation
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'user_id_pic' => 'required|exists:users,id',
        ]);

        // $user_lab_find = UserLab::where('user_id', $user->id)->first();

        $lab = Lab::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id_pic' => $request->user_id_pic,
        ]);

        $user_lab = UserLab::create([
            'user_id' => $request->user_id_pic,
            'lab_id' => $lab->id,
        ]);

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], null, json_encode($request->all()), '-');

        return $this->successResponse([
            'lab' => $lab
        ], "Create Data Success!");
    }

    // ONLY ADMIN & USER PIC
    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        if (!($role->name === 'admin' OR $role->name === 'dosen')) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // check if user is pic
        $lab = Lab::find($id);
        if ($lab->user_id_pic !== $user->id) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // Validation
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'user_id_pic' => 'required|exists:users,id',
        ]);

        // check if prev pic is the same as new pic
        if ($lab->user_id_pic !== $request->user_id_pic) {
            // check if new pic already a laboran
            $user_lab = UserLab::where('lab_id', $lab->id)->where('user_id', $request->user_id_pic)->first();
            if (is_null($user_lab)) {
                $user_lab = UserLab::create([
                    'user_id' => $request->user_id_pic,
                    'lab_id' => $lab->id,
                ]);
            }
        }

        $lab->name = $request->name;
        $lab->description = $request->description;
        $lab->user_id_pic = $request->user_id_pic;
        $lab->save();

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');

        return $this->successResponse([
            'lab' => $lab
        ], "Update Data Success!");
    }

    public function destroy(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        if ($role->name !== 'admin' ) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        $lab = Lab::find($id); // bisa pake findOrFail
        $lab->delete(); // bisa pake if ($lab) buat pengecekan findOrFail // bisa implement soft delete

        //find list item
        $item_find = Item::where('lab_id', $lab->id)->get();
        $list_item_id = array();
        foreach ($item_find as $x) {
            array_push($list_item_id, $x['id']);
        }

        // delete transaksi item
        $item = Item::where('lab_id', $lab->id)->update(['deleted_at' => now()]);
        // delete transaksi pinjam
        Borrow::whereIn('item_id', $list_item_id)->update(['deleted_at' => now()]);

        // return $this->errorResponse($list_item_id, "Delete Data Success!", 401);

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');

        return $this->successResponse([], "Delete Data Success!");
    }
}
