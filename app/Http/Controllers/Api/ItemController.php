<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\Borrow;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Item;
use App\Models\Lab;
use App\Models\UserLab;
use App\Traits\log;
use App\Models\ItemImage;

class ItemController extends Controller
{
    use log, HttpResponses;

    public function index()
    {
        return $this->successResponse(Item::with('lab', 'item_image')->get());
    }

    public function show($id)
    {
        return $this->successResponse(Item::with('lab', 'item_image')->find($id));
    }

    // ONLY ADMIN & USER LABORAN
    public function store(Request $request)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        // check role
        $role = Role::find($user_role->role_id);
        if (!($role->name === 'admin' OR $role->name === 'dosen')) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // Validation
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'lab_id' => 'required|exists:labs,id',
            'stock' => 'required|numeric|min:0',
            'remaining' => 'required|numeric|min:0',
        ]);

        if ($role->name !== 'admin') {
            // check if user is already laboran
            $user_lab = UserLab::where('lab_id', $request->lab_id)->where('user_id', $user->id)->first();
            if (is_null($user_lab)) {
                return $this->errorResponse('', 'User does not have the access to this function!', 400);
            }
        }

        // check stock & remaining
        if ($request->remaining > $request->stock) {
            return $this->errorResponse('', 'Sisa tidak bisa melebihi Stok!', 400);
        }

        $item = Item::create([
            'name' => $request->name,
            'description' => $request->description,
            'lab_id' => $request->lab_id,
            'stock' => $request->stock,
            'remaining' => $request->remaining,
        ]);

        $text = "lampiran";
        $arr = array_flip(preg_grep("/$text/", array_keys((array)$request->all())));
        $arr_temp = [];

        for ($i=0; $i < count($arr); $i++) {
            if ($request['lampiran-'.$i] != null AND $request['lampiran-'.$i] != "") {
                $path_temp = $request->file('lampiran-'.$i)->store('item', 'public');
                array_push(
                    $arr_temp,
                    [
                        'item_id' => $item->id,
                        'url_path' => $path_temp,
                        'created_by' => $user->id,
                        'updated_by' => $user->id,
                        'created_at' => new \DateTime(),
                        'updated_at' => new \DateTime()
                    ]
                );
            }
        }
        ItemImage::insert($arr_temp);

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], null, json_encode($request->all()), '-');

        return $this->successResponse([
            'item' => $item
        ], "Create Data Success!");
    }

    // ONLY ADMIN & USER LABORAN
    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }

        // check role
        $role = Role::find($user_role->role_id);
        if (!($role->name === 'admin' OR $role->name === 'dosen')) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // Validation
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'lab_id' => 'required|exists:labs,id',
            'stock' => 'required|numeric',
            'remaining' => 'required|numeric',
        ]);

        if ($role->name !== 'admin') {
            // check if user is already laboran
            $user_lab = UserLab::where('lab_id', $request->lab_id)->where('user_id', $user->id)->first();
            if (is_null($user_lab)) {
                return $this->errorResponse('', 'User does not have the access to this function!', 400);
            }
        }

        // check stock & remaining
        if ($request->remaining > $request->stock) {
            return $this->errorResponse('', 'Sisa tidak bisa melebihi Stok!', 400);
        }

        $item = Item::find($id);
        $item->name = $request->name;
        $item->description = $request->description;
        $item->lab_id = $request->lab_id;
        $item->stock = $request->stock;
        $item->is_available = $request->is_available;
        $item->remaining = $request->remaining;


        $arr = array_flip(preg_grep("/\lampiran/", array_keys((array)$request->all())));
        $arr_temp = [];

        for ($i=0; $i < count($arr); $i++) {
            if ($request['lampiran-'.$i] != null AND $request['lampiran-'.$i] != "") {
                $path_temp = $request->file('lampiran-'.$i)->store('item', 'public');
                array_push(
                    $arr_temp,
                    [
                        'item_id' => $id,
                        'url_path' => $path_temp,
                        'created_by' => $user->id,
                        'updated_by' => $user->id,
                        'created_at' => new \DateTime(),
                        'updated_at' => new \DateTime()
                    ]
                );
            }
        }
        ItemImage::insert($arr_temp);

        $item->save();

        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');

        return $this->successResponse([
            'item' => $item,
        ], "Update Data Success!");
    }

    public function destroy(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        // check role
        $role = Role::find($user_role->role_id);
        if (!($role->name === 'admin' OR $role->name === 'dosen')) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // find item
        $item = Item::find($id); // bisa pake findOrFail

        if ($role->name !== 'admin') {
            // check if user is already laboran
            $user_lab = UserLab::where('lab_id', $item->lab_id)->where('user_id', $user->id)->first();
            if (is_null($user_lab)) {
                return $this->errorResponse('', 'User does not have the access to this function!', 400);
            }
        }

        $item->delete(); // bisa pake if ($lab) buat pengecekan findOrFail // bisa implement soft delete
        Borrow::where('item_id', $item->id)->update(['deleted_at' => now()]);
        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');

        return $this->successResponse([], "Delete Data Success!");
    }

    public function deleteItemImage(Request $request, $id)
    {
        $user = auth()->user();
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse('', 'User does not have the selected role!', 401);
        }
        // check role
        $role = Role::find($user_role->role_id);
        if (!($role->name === 'admin' OR $role->name === 'dosen')) {
            return $this->errorResponse('', 'User does not have the access to this function!', 400);
        }

        // find item
        $item_image = ItemImage::find($id); // bisa pake findOrFail
        $item = Item::find($item_image->item_id);

        if ($role->name !== 'admin') {
            // check if user is already laboran
            $user_lab = UserLab::where('lab_id', $item->lab_id)->where('user_id', $user->id)->first();
            if (is_null($user_lab)) {
                return $this->errorResponse('', 'User does not have the access to this function!', 400);
            }
        }

        $item_image->delete(); // bisa pake if ($lab) buat pengecekan findOrFail // bisa implement soft delete
        $this->saveLog(get_class($this), explode('@', Route::getCurrentRoute()->getActionName())[1], $id, json_encode($request->all()), '-');

        return $this->successResponse([], "Delete Data Success!");
    }
}
