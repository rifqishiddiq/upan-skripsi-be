<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\UserLab;

class AuthController extends Controller
{
    use HttpResponses;

    public function login(Request $request)
    {
        // Validation
        $credentials = $request->validate([
            // 'email' => 'required|email',
            'nim_nidn' => 'required|numeric',
            'password' => 'required',
        ]);

        // Check User
        if (!auth()->attempt($credentials)) {
            return $this->errorResponse('', 'Your credential does not match!', 401);
        }
        $user = auth()->user();

        // Check Role
        $user_role = UserRole::where('user_id', $user->id)->first();
        if ($user_role === null) {
            return $this->errorResponse([$user->id, $request->role_id], 'User does not have the selected role!', 401);
        }
        $role = Role::find($user_role->role_id);
        $lab = UserLab::select('lab_id')->where('user_id', $user->id)->get();

        return $this->successResponse([
            'user' => $user,
            'role' => $role,
            'lab' => $lab,
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
        
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'nim_nidn' =>'required|digits:10|numeric|unique:users',
            'name' =>'required|max:255',
            'phone' =>'required|numeric|digits_between:3,15|unique:users',
            'email' =>'required|email|max:255|unique:users',
            'password' =>'required_with:password_confirmation|min:6|confirmed',
            'role_id' =>'required|exists:roles,id',
        ]);

        $user = User::create([
            'nim_nidn' => $request->nim_nidn,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $request->role_id,
        ]);

        return $this->successResponse([
            'user' => $user,
            // 'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    public function logout()
    {
        // User::where('id', auth()->user()->id)->update(['role_id_active' => null]);
        auth()->user()->currentAccessToken()->delete();

        return $this->successResponse([
            'message' => 'User logged out Successfully!',
        ]);
    }

    public function checkIfAuthenticated()
    {
        if (auth()->check()) {
            $user = auth()->user();
            // Check Role
            $user_role = UserRole::where('user_id', $user->id)->first();
            // $user_role = UserRole::where('user_id', $user->id)->where('role_id', $user->role_id_active)->first();
            if ($user_role === null) {
                return $this->errorResponse('', 'User does not have the selected role!', 401);
            }
            $role = Role::find($user_role->role_id);
            $lab = UserLab::select('lab_id')->where('user_id', $user->id)->get();

            return $this->successResponse([
                'user' => $user,
                'role' => $role,
                'lab' => $lab,
                'token' => $user->createToken('API Token')->plainTextToken,
                'message' => 'You are Authenticated!',
            ]);
        } else {
            return $this->errorResponse('', 'You are not Authenticated', 401);
        }
    }
}
