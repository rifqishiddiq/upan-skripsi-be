<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\CreatedUpdatedBy;
use App\Models\User;
use App\Models\Item;
use App\Models\BorrowStatus;

class Borrow extends Model
{
    use HasFactory, SoftDeletes, CreatedUpdatedBy;

    protected $fillable = [
        'user_id_borrower',
        'user_id_approved',
        'user_id_giver',
        'user_id_receiver',
        'item_id',
        'qty_borrowed',
        'qty_returned',
        'borrow_date',
        'return_date',
        'deadline_date',
        'note',
        'status_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at'
    ];

    public function borrower()
    {
        return $this->hasOne(User::class, 'id', 'user_id_borrower');
    }

    public function approver()
    {
        return $this->hasOne(User::class, 'id', 'user_id_approved');
    }

    public function laboran_giver()
    {
        return $this->hasOne(User::class, 'id', 'user_id_giver');
    }

    public function laboran_receiver()
    {
        return $this->hasOne(User::class, 'id', 'user_id_receiver');
    }

    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }

    public function status()
    {
        return $this->hasOne(BorrowStatus::class, 'id', 'status_id');
    }
}
