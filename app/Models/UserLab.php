<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLab extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'lab_id',
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'deleted_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function lav()
    {
        return $this->hasOne(User::class, 'id', 'lab_id');
    }
}
