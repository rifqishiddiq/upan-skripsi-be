<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\CreatedUpdatedBy;

class BorrowStatus extends Model
{
    use HasFactory, SoftDeletes, CreatedUpdatedBy;
    
    protected $table = 'borrow_status';

    protected $fillable = [
        'name',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at'
    ];
}
