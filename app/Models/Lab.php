<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\CreatedUpdatedBy;
use App\Models\User;
use App\Models\UserLab;

class Lab extends Model
{
    use HasFactory, SoftDeletes, CreatedUpdatedBy;

    protected $fillable = [
        'name',
        'description',
        'user_id_pic',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at'
    ];

    public function pic()
    {
        return $this->hasOne(User::class, 'id', 'user_id_pic');
    }

    public function user_lab()
    {
        return $this->hasMany(UserLab::class, 'lab_id', 'id');
    }
}
