<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\CreatedUpdatedBy;
use App\Models\Lab;
use App\Models\ItemImage;

class Item extends Model
{
    use HasFactory, SoftDeletes, CreatedUpdatedBy;

    protected $fillable = [
        'name',
        'description',
        'stock',
        'remaining',
        'lab_id',
        'is_available',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at'
    ];

    public function lab()
    {
        return $this->hasOne(Lab::class, 'id', 'lab_id');
    }

    public function item_image()
    {
        return $this->hasMany(ItemImage::class, 'item_id', 'id');
    }
}
