<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\CreatedBy;

class Log extends Model
{
    use HasFactory, CreatedBy;

    const UPDATED_AT = null;

    protected $fillable = [
        'class_name',
        'action_name',
        'id_data',
        'data',
        'response',
    ];

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}

