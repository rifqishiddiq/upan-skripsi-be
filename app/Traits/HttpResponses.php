<?php

namespace App\Traits;

trait HttpResponses
{
    /**
     * Building success response
     * @param $data
     * @param int $code
     * @return JsonResponse
     */
    public function successResponse($data, $message = null, $code = 200)
    {
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => $message,
        ], $code);
    }

    /**
     * Building success response
     * @param $data
     * @param int $code
     * @return JsonResponse
     */
     public function errorResponse($data, $message = null, $code)
    {
        return response()->json([
            'success' => false,
            'data' => $data,
            'message' => $message,
        ], $code);
    }
}