<?php

namespace App\Traits;
use App\Models\Log as logApp;

trait log
{
    public static function saveLog($className, $actName, $idData, $data, $response){
        logApp::create([
            'class_name'            => $className,
            'action_name'           => $actName,
            'id_data'               => $idData,
            'data'                  => $data,
            'response'              => $response,
        ]);
    }
}